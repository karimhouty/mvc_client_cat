var app = angular.module("MyApp", ['ui.router']);

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state("produits", {
        url:"/produits",
        templateUrl:"views/produits.html",
        controller:"ProduitController"
    }).state("categories", {
        url:"/categories",
        templateUrl:"views/categories.html",
        controller:"CategorieController"
    });

    $urlRouterProvider.otherwise("produits");

});

app.controller("CategorieController", function () {

});

app.controller("ProduitController", function ($scope, $http) {


    $scope.name = "";
    $scope.prevName = "";
    $scope.size = 5;
    $scope.pageCourante = 0;
    $scope.pages = [];
    //$scope.produits = null;
    $scope.chercher = function () {
        $http({
            method: 'GET',
            url: 'http://localhost:8080/chercher?mc=' + $scope.name + '&page=' + $scope.pageCourante + '&size=' + $scope.size
        }).then(function (data) {
            $scope.produits = data.data.content;
            $scope.pages = new Array(data.data.totalPages);
            console.log(data);
            if ($scope.name !== $scope.prevName) {
                $scope.pageCourante = 0;
                $scope.chercher();
            }
        })
    }

    $scope.goToPage = function (p) {
        $scope.pageCourante = p;
        $scope.chercher();
        $scope.prevName = $scope.name;
    }

});