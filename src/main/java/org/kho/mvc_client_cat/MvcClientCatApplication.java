package org.kho.mvc_client_cat;

import org.kho.mvc_client_cat.org.kho.dao.ProduitRepository;
import org.kho.mvc_client_cat.org.kho.entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcClientCatApplication implements CommandLineRunner {

    @Autowired
    private ProduitRepository produitRepository;

    public static void main(String[] args) {
        SpringApplication.run(MvcClientCatApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        produitRepository.save(new Produit(null, "HP 678", 900, 6));
        produitRepository.save(new Produit(null, "Samsung Galaxy", 905, 7));
        produitRepository.save(new Produit(null, "Xerox", 5600, 6));
    }
}
