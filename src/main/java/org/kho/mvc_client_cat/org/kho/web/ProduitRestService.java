package org.kho.mvc_client_cat.org.kho.web;

import org.kho.mvc_client_cat.org.kho.dao.ProduitRepository;
import org.kho.mvc_client_cat.org.kho.entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProduitRestService {

    @Autowired
    private ProduitRepository produitRepository;

    @RequestMapping(value = "/produits", method = RequestMethod.GET)
    public List<Produit> listProduits(){
        return produitRepository.findAll();
    }

    @RequestMapping(value = "/chercher", method = RequestMethod.GET)
    public Page<Produit> chercher(
            @RequestParam(name = "mc", defaultValue = "") String mc,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "5") int size){
        return produitRepository.chercher("%"+mc+"%", PageRequest.of(page, size));
    }

    @RequestMapping(value = "/produits/{id}", method = RequestMethod.GET)
    public Optional<Produit> getProduit(@PathVariable(name = "id")Long id){
        return produitRepository.findById(id);
    }

    @RequestMapping(value = "/produits", method = RequestMethod.POST)
    public Produit save(@RequestBody Produit p){
        return produitRepository.save(p);
    }

    @RequestMapping(value = "/produits/{id}", method = RequestMethod.PUT)
    public Produit update(@PathVariable(name = "id") Long id ,@RequestBody Produit p){
        p.setId(id);
        return produitRepository.saveAndFlush(p);
    }

}
