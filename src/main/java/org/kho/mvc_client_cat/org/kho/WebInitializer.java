package org.kho.mvc_client_cat.org.kho;

import org.kho.mvc_client_cat.MvcClientCatApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class WebInitializer extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(MvcClientCatApplication.class);
    }
}
